import { Component, OnInit } from '@angular/core';
import { DataHandlerService } from '../data-handler.service';
import { Category } from '../model/Category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
})
export class CategoriesComponent implements OnInit {
  categories: Category[];
  selectedCategory: Category

  constructor(private dataHandler: DataHandlerService) {}
  // метод вызывается автоматически посли инициализации компонента
  ngOnInit() {
    this.dataHandler.categoriesSubject.subscribe( categories => this.categories = categories); // Вызываем подписчика с данными и передаем их кмпоненту
  }

  showTasksByCategory(category: Category) {
    // Здесь хранится все что отправляем в HTML
    this.selectedCategory = category;
    this.dataHandler.fillTasksByCategory(category)
  }
}
