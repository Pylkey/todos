import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { TestData } from './data/TestData';
import { Category } from './model/Category';
import { Task } from './model/Task';

@Injectable({
  providedIn: 'root'
})
export class DataHandlerService {
  // BehaviorSubject требует заполнить себя полными данными
  tasksSubject = new BehaviorSubject<Task[]>(TestData.tasks) // Обозреватель (за кем следят)
  categoriesSubject = new BehaviorSubject<Category[]> (TestData.categories) // Обозреватель (за кем следят)

  constructor() { 
    this.fillTask();
  }

  fillTask() {
    this.tasksSubject.next(TestData.tasks); // Подписчик обозревателя (следят за изменившимися даными)
  }

  fillTasksByCategory(category: Category) {
    const tasks = TestData.tasks.filter(task => task.category === category);
    this.tasksSubject.next(tasks); // Подписчик обозревателя (следят за изменившимися даными)
  } 

}
