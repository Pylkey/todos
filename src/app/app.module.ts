// В этом файле описываем какие компоненты мы будем импортировать в наш проект

// Этот блок описывает, какие компоненты будем использовать и указываем путь до них
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CategoriesComponent } from './categories/categories.component';
import { TasksComponent } from './tasks/tasks.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Импортируем модули в наш проект
@NgModule({
  // Создаем свои модули в проекте
  declarations: [
    AppComponent,
    CategoriesComponent,
    TasksComponent
  ],
  // Импортируем выбранные ранее модули
  imports: [
    BrowserModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    BrowserAnimationsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
// Экспортируем все в класс нашего приложения
export class AppModule { }
